$('.js-show-menu').on('click', function() {
  $('.header').toggleClass('is-menu-visible');
})

$('.js-show-lang').on('click', function() {
  $(this).toggleClass('is-menu-visible');
})

$('.js-open-filters').on('click', function(e) {
  e.preventDefault();
  let parent = $(this).parents('.header-inner-filters');
  parent.toggleClass('is-open');
  setTimeout(function() {
    $('.filters-content').slimScroll({
      height: '100vh',
      size: '15px',
      color: '#6e6e6e',
      alwaysVisible: true,
      railVisible: true,
      railColor: '#595959',
      railOpacity: 1,
    });
  }, 20)
})

$('.filters-category').slimScroll({
  height: '390px',
  size: '15px',
  color: '#6e6e6e',
  alwaysVisible: true,
  railVisible: true,
  railColor: '#595959',
  railOpacity: 1,
});

$('.filters-cameras').slimScroll({
  height: '244px',
  size: '15px',
  color: '#6e6e6e',
  alwaysVisible: true,
  railVisible: true,
  railColor: '#595959',
  railOpacity: 1,
});