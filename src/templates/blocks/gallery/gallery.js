
$('.gallery__item').mouseenter(function() {

  let windowWidth = $(window).width();

  if (windowWidth >= 768) {
    let container = $(this).parents('.container').outerWidth() + 95;
    let fix = windowWidth - container;
    let a = $(this).offset().left;
    let width = $(this).find('.gallery__info').width();
    let free = windowWidth - (a + width);
    let tooltip = $(this).find('.gallery__info');
    tooltip.addClass('is-visible');

    let images = $(this).find('.gallery__slides-container img')
    $(this).find('.gallery__slides-container').css({
      'width': (images.length/3) * 100 + '%'
    })
    let slidesWidth = $(this).find('.gallery__slides').width().toFixed();
    images.css({
      width: ((slidesWidth - 96)/12).toFixed + 'px'
    })

    tooltip.css({
      top: '-' + tooltip.height() + 'px'
    });
    if (free <= 0) {
      let offset = free - fix
      $(this).find('.gallery__info').css({
        left: `${offset}px`
      })
    }

  }

})

$('.gallery__item').mouseleave(function() {
  let tooltip = $(this).find('.gallery__info');
  tooltip.removeClass('is-visible');
  tooltip.css({
    top: '0'
  });
})

$('body').on('click', '.js-gallery-prev', function(e) {
  e.preventDefault()
  let galleryOffset = $(this).parent().find('.gallery__slides').width().toFixed();
  let target = $(this).parent().find('.gallery__slides-container');
  let currentPos = parseInt(target.css('left'), 10);
  if (currentPos === 0) {
    return false
  } else {
    let val = currentPos + parseInt(galleryOffset, 10);
    target.css({
      'left': val + 'px'
    });
  }
})

$('body').on('click', '.js-gallery-next', function(e) {
  e.preventDefault()
  let galleryOffset = $(this).parent().find('.gallery__slides').width().toFixed();
  let target = $(this).parent().find('.gallery__slides-container');
  let currentPos = parseInt(target.css('left'), 10);
  let max = $(this).parent().find('.gallery__slides-container img').length/3 -1;
  if (currentPos === galleryOffset * max * -1) {
    return false
  } else {
    let val = currentPos - parseInt(galleryOffset, 10);
    target.css({
      'left': val + 'px'
    });
  }

})
