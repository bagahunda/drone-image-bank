const footerAnchor = $('.footer h3');
footerAnchor.on('click', function() {
  let windowWidth = $(window).width();
  if (windowWidth >= 768) {
    return false;
  }
  $(this).next('ul').toggleClass('is-visible');
})
